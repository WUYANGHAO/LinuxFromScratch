# <a href="https://gitlab.com/WUYANGHAO/LinuxFromScratch">首页</a>
# 编译工具链
*LFS 8.2*
## 一、编译临时系统
使用lfs用户进行编译
```bash
su - lfs
```
#### 1、Binutils-2.30 - Pass 1
```bash
tar xvf binutils-2.30.tar.xz
cd binutils-2.30
mkdir -v build
cd       build
../configure --prefix=/tools            \
             --with-sysroot=$LFS        \
             --with-lib-path=/tools/lib \
             --target=$LFS_TGT          \
             --disable-nls              \
             --disable-werror
make

case $(uname -m) in
  x86_64) mkdir -v /tools/lib && ln -sv lib /tools/lib64 ;;
esac
make install

cd $LFS/sources
rm -rf binutils-2.30
```
#### 2、GCC-7.3.0 - Pass 1
```bash
tar xvf gcc-7.3.0.tar.xz
cd gcc-7.3.0
tar -xf ../mpfr-4.0.1.tar.xz
mv -v mpfr-4.0.1 mpfr
tar -xf ../gmp-6.1.2.tar.xz
mv -v gmp-6.1.2 gmp
tar -xf ../mpc-1.1.0.tar.gz
mv -v mpc-1.1.0 mpc
for file in gcc/config/{linux,i386/linux{,64}}.h
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
 ;;
esac
mkdir -v build
cd       build
../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=/tools                                \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --with-local-prefix=/tools                     \
    --with-native-system-header-dir=/tools/include \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libmpx                               \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++
make
make install

cd $LFS/sources
rm -rf gcc-7.3.0
```
#### 3、Linux-4.15.3 API Headers
```bash
tar xvf linux-4.15.3.tar.xz
cd linux-4.15.3
make mrproper
make INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* /tools/include

cd $LFS/sources
rm -rf linux-4.15.3
```
#### 4、Glibc-2.27
```bash
tar xvf glibc-2.27.tar.xz
cd glibc-2.27
mkdir -v build
cd       build
../configure                             \
      --prefix=/tools                    \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2             \
      --with-headers=/tools/include      \
      libc_cv_forced_unwind=yes          \
      libc_cv_c_cleanup=yes
make
make install

echo 'int main(){}' > dummy.c
$LFS_TGT-gcc dummy.c
readelf -l a.out | grep ': /tools'

//[Requesting program interpreter: /tools/lib64/ld-linux-x86-64.so.2]

cd $LFS/sources
rm -rf glibc-2.27
```
#### 5、Libstdc++-7.3.0（gcc内的一部分）
```bash
tar xvf gcc-7.3.0.tar.xz
cd gcc-7.3.0
mkdir -v build
cd       build
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --prefix=/tools                 \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-threads     \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/7.3.0
make
make install

cd $LFS/sources
rm -rf gcc-7.3.0
```
#### 6、Binutils-2.30 - Pass 2
```bash
tar xvf binutils-2.30.tar.xz
cd binutils-2.30
mkdir -v build
cd       build
CC=$LFS_TGT-gcc                \
AR=$LFS_TGT-ar                 \
RANLIB=$LFS_TGT-ranlib         \
../configure                   \
    --prefix=/tools            \
    --disable-nls              \
    --disable-werror           \
    --with-lib-path=/tools/lib \
    --with-sysroot
make
make install
make -C ld clean
make -C ld LIB_PATH=/usr/lib:/lib
cp -v ld/ld-new /tools/bin

cd $LFS/sources
rm -rf binutils-2.30
```
#### 7、GCC-7.3.0 - Pass 2
```bash
tar xvf gcc-7.3.0.tar.xz
cd gcc-7.3.0
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
  `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/include-fixed/limits.h
for file in gcc/config/{linux,i386/linux{,64}}.h
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
esac
tar -xf ../mpfr-4.0.1.tar.xz
mv -v mpfr-4.0.1 mpfr
tar -xf ../gmp-6.1.2.tar.xz
mv -v gmp-6.1.2 gmp
tar -xf ../mpc-1.1.0.tar.gz
mv -v mpc-1.1.0 mpc
mkdir -v build
cd       build
CC=$LFS_TGT-gcc                                    \
CXX=$LFS_TGT-g++                                   \
AR=$LFS_TGT-ar                                     \
RANLIB=$LFS_TGT-ranlib                             \
../configure                                       \
    --prefix=/tools                                \
    --with-local-prefix=/tools                     \
    --with-native-system-header-dir=/tools/include \
    --enable-languages=c,c++                       \
    --disable-libstdcxx-pch                        \
    --disable-multilib                             \
    --disable-bootstrap                            \
    --disable-libgomp
make
make install
ln -sv gcc /tools/bin/cc

echo 'int main(){}' > dummy.c
cc dummy.c
readelf -l a.out | grep ': /tools'

//[Requesting program interpreter: /tools/lib64/ld-linux.so.2]

cd $LFS/sources
rm -rf gcc-7.3.0
```
#### 8、Tcl-core-8.6.8
```bash
tar zxvf tcl8.6.8-src.tar.gz
cd tcl8.6.8
cd unix
./configure --prefix=/tools
make
TZ=UTC make test
make install
chmod -v u+w /tools/lib/libtcl8.6.so
make install-private-headers
ln -sv tclsh8.6 /tools/bin/tclsh

cd $LFS/sources
rm -rf tcl8.6.8
```
#### 9、Expect-5.45.4
```bash
tar zxvf expect5.45.4.tar.gz
cd expect5.45.4
cp -v configure{,.orig}
sed 's:/usr/local/bin:/bin:' configure.orig > configure
./configure --prefix=/tools       \
            --with-tcl=/tools/lib \
            --with-tclinclude=/tools/include
make
make test
make SCRIPTS="" install

cd $LFS/sources
rm -rf expect5.45.4
```
#### 10、DejaGNU-1.6.1
```bash
tar zxvf dejagnu-1.6.1.tar.gz
cd dejagnu-1.6.1
./configure --prefix=/tools
make install

cd $LFS/sources
rm -rf dejagnu-1.6.1
```
#### 11、M4-1.4.18
```bash
tar xvf m4-1.4.18.tar.xz
cd m4-1.4.18
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf m4-1.4.18
```
#### 12、Ncurses-6.1
```bash
tar zxvf ncurses-6.1.tar.gz
cd ncurses-6.1
sed -i s/mawk// configure
./configure --prefix=/tools \
            --with-shared   \
            --without-debug \
            --without-ada   \
            --enable-widec  \
            --enable-overwrite
make
make install

cd $LFS/sources
rm -rf ncurses-6.1
```
#### 13、Bash-4.4.18
```bash
tar zxvf bash-4.4.18.tar.gz
cd bash-4.4.18
./configure --prefix=/tools --without-bash-malloc
make
make install
ln -sv bash /tools/bin/sh

cd $LFS/sources
rm -rf bash-4.4.18
```
#### 14、Bison-3.0.4
```bash
tar xvf bison-3.0.4.tar.xz
cd bison-3.0.4
./configure --prefix=/tools
make

make install

cd $LFS/sources
rm -rf bison-3.0.4
```
#### 15、Bzip2-1.0.6
```bash
tar zxvf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make
make PREFIX=/tools install

cd $LFS/sources
rm -rf bzip2-1.0.6
```
#### 16、Coreutils-8.29
```bash
tar xvf coreutils-8.29.tar.xz
cd coreutils-8.29
./configure --prefix=/tools --enable-install-program=hostname
make
make install

cd $LFS/sources
rm -rf coreutils-8.29
```
#### 17、Diffutils-3.6
```bash
tar xvf diffutils-3.6.tar.xz
cd diffutils-3.6
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf diffutils-3.6
```
#### 18、File-5.32
```bash
tar zxvf file-5.32.tar.gz
cd file-5.32
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf file-5.32
```
#### 19、Findutils-4.6.0
```bash
tar zxvf findutils-4.6.0.tar.gz
cd findutils-4.6.0
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf findutils-4.6.0
```
#### 20、Gawk-4.2.0
```bash
tar xvf gawk-4.2.0.tar.xz
cd gawk-4.2.0
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf gawk-4.2.0
```
#### 21、Gettext-0.19.8.1
```bash
tar xvf gettext-0.19.8.1.tar.xz
cd gettext-0.19.8.1
cd gettext-tools
EMACS="no" ./configure --prefix=/tools --disable-shared
make -C gnulib-lib
make -C intl pluralx.c
make -C src msgfmt
make -C src msgmerge
make -C src xgettext
cp -v src/{msgfmt,msgmerge,xgettext} /tools/bin

cd $LFS/sources
rm -rf gettext-0.19.8.1
```
#### 22、Grep-3.1
```bash
tar xvf grep-3.1.tar.xz
cd grep-3.1
./configure --prefix=/tools
make 
make install

cd $LFS/sources
rm -rf grep-3.1
```
#### 23、Gzip-1.9
```bash
tar xvf gzip-1.9.tar.xz
cd gzip-1.9
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf gzip-1.9
```
#### 24、Make-4.2.1
```bash
tar xvf make-4.2.1.tar.bz2
cd make-4.2.1
sed -i '211,217 d; 219,229 d; 232 d' glob/glob.c
./configure --prefix=/tools --without-guile
make

make install

cd $LFS/sources
rm -rf make-4.2.1
```
#### 25、Patch-2.7.6
```bash
tar xvf patch-2.7.6.tar.xz
cd patch-2.7.6
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf patch-2.7.6
```
#### 26、Perl-5.26.1
```bash
tar xvf perl-5.26.1.tar.xz
cd perl-5.26.1
sh Configure -des -Dprefix=/tools -Dlibs=-lm
make
cp -v perl cpan/podlators/scripts/pod2man /tools/bin
mkdir -pv /tools/lib/perl5/5.26.1
cp -Rv lib/* /tools/lib/perl5/5.26.1

cd $LFS/sources
rm -rf perl-5.26.1
```
#### 27、Sed-4.4
```bash
tar xvf sed-4.4.tar.xz
cd sed-4.4
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf sed-4.4
```
#### 28、Tar-1.30
```bash
tar xvf tar-1.30.tar.xz
cd tar-1.30
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf tar-1.30
```
#### 29、Texinfo-6.5
```bash
tar xvf texinfo-6.5.tar.xz
cd texinfo-6.5
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf texinfo-6.5
```
#### 30、Util-linux-2.31.1
```bash
tar xvf util-linux-2.31.1.tar.xz
cd util-linux-2.31.1
./configure --prefix=/tools                \
            --without-python               \
            --disable-makeinstall-chown    \
            --without-systemdsystemunitdir \
            --without-ncurses              \
            PKG_CONFIG=""
make
make install

cd $LFS/sources
rm -rf util-linux-2.31.1
```
#### 31、Xz-5.2.3
```bash
tar xvf xz-5.2.3.tar.xz
cd xz-5.2.3
./configure --prefix=/tools
make
make install

cd $LFS/sources
rm -rf xz-5.2.3
```
## 二、剥离
```bash
strip --strip-debug /tools/lib/*
/usr/bin/strip --strip-unneeded /tools/{,s}bin/*
rm -rf /tools/{,share}/{info,man,doc}
find /tools/{lib,libexec} -name \*.la -delete
```
## 三、修改工具权限
```bash
exit
chown -R root:root $LFS/tools
```
====================================================================================================================

[[上一章](002-准备编译软件包.md)]   [[目录](/README.md)]    [[下一章](004-编译操作系统.md)]