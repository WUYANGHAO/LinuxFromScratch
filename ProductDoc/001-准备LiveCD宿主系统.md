# <a href="https://gitlab.com/WUYANGHAO/LinuxFromScratch">首页</a>
# 准备LiveCD宿主系统
*以ubuntu为例*
### 1、下载Ubuntu系统
<a href="https://www.ubuntu.com/download/desktop">Ubuntu官网</a>
### 2、下载VMware Workstation
<a href="https://my.vmware.com/en/web/vmware/info/slug/desktop_end_user_computing/vmware_workstation_pro/14_0">vmware workstation_pro</a>
### 3、安装宿主系统（过程不赘述）
##### (1) 安装VMware Workstation

##### (2) 使用VMware创建一个空白的虚拟机

【注】：选择其他kernel 4.X；内存和CPU可以高一点，加快编译响应速度；磁盘10G就足够了

##### (3) 挂载下载好的Ubuntu镜像，开机，选择“试用”————不要安装
### 4、配置宿主系统
##### (1) 配置软件源：
图形化：打开软件和更新工具，选择中国区的资源，提交；
命令行：上网查找源，修改/etc/apt/source.list,保存后执行如下命令
```bash
apt-get update
apt-get upgrade
```
##### (2) 安装必要软件 vim openssh-server
```bash
apt-get install vim
apt-get install openssh-server
```
##### (3) 新建分区并挂载——VMware默认是/dev/sda（后续都在命令行操作）
I) 创建/dev/sda1 分区，执行以下命令，交互输入 n,p,回车,回车,w,q
```bash
fdisk /dev/sda
```
II) 格式化/dev/sda1 分区
```bash
mkfs -v -t ext4 /dev/sda1
```
III) 创建lfs目录并挂载分区
```bash
export LFS=/mnt/lfs
mkdir -pv $LFS
mount -v -t ext4 /dev/sda1 $LFS
```
IV) 创建编译工具目录
```bash
mkdir -v $LFS/tools
ln -sv $LFS/tools /
mkdir -v $LFS/sources
```
##### (4) 创建、设置操作用户和组
```bash
groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs
passwd lfs
```
修改目录权限
```bash
chown -v lfs $LFS/tools
chown -v lfs $LFS/sources
chmod -v a+wt $LFS/sources
```
切换到lfs用户
```bash
su - lfs
```
创建lfs用户配置
```bash
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF
```
```bash
cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH
EOF
```
使配置生效
```bash
source ~/.bash_profile
```
##### (5) 安装编译所需工具包
I) 检查系统工具是否存在
创建检查脚本
```bash
cat > version-check.sh << "EOF"
#!/bin/bash
# Simple script to list version numbers of critical development tools
export LC_ALL=C
bash --version | head -n1 | cut -d" " -f2-4
MYSH=$(readlink -f /bin/sh)
echo "/bin/sh -> $MYSH"
echo $MYSH | grep -q bash || echo "ERROR: /bin/sh does not point to bash"
unset MYSH

echo -n "Binutils: "; ld --version | head -n1 | cut -d" " -f3-
bison --version | head -n1

if [ -h /usr/bin/yacc ]; then
  echo "/usr/bin/yacc -> `readlink -f /usr/bin/yacc`";
elif [ -x /usr/bin/yacc ]; then
  echo yacc is `/usr/bin/yacc --version | head -n1`
else
  echo "yacc not found" 
fi

bzip2 --version 2>&1 < /dev/null | head -n1 | cut -d" " -f1,6-
echo -n "Coreutils: "; chown --version | head -n1 | cut -d")" -f2
diff --version | head -n1
find --version | head -n1
gawk --version | head -n1

if [ -h /usr/bin/awk ]; then
  echo "/usr/bin/awk -> `readlink -f /usr/bin/awk`";
elif [ -x /usr/bin/awk ]; then
  echo awk is `/usr/bin/awk --version | head -n1`
else 
  echo "awk not found" 
fi

gcc --version | head -n1
g++ --version | head -n1
ldd --version | head -n1 | cut -d" " -f2-  # glibc version
grep --version | head -n1
gzip --version | head -n1
cat /proc/version
m4 --version | head -n1
make --version | head -n1
patch --version | head -n1
echo Perl `perl -V:version`
sed --version | head -n1
tar --version | head -n1
makeinfo --version | head -n1
xz --version | head -n1

echo 'int main(){}' > dummy.c && g++ -o dummy dummy.c
if [ -x dummy ]
  then echo "g++ compilation OK";
  else echo "g++ compilation failed"; fi
rm -f dummy.c dummy
EOF
```
执行检查脚本
```bash
bash version-check.sh
```
II) 上一步有问题的解决方法
<1>软件包缺失
```bash
apt-get install <software naem>         //在Ubuntu系统中makeinfo应该是texinfo
```
<2>bash链接
```bash
dpkg-reconfigure dash          //选择“否”
```
====================================================================================================================

[[首页](/README.md)]    [[目录](/README.md)]    [[下一章](002-准备编译软件包.md)]