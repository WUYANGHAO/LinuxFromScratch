# <a href="https://gitlab.com/WUYANGHAO/LinuxFromScratch">Linux From Scratch</a>
*版本 ：8.2*

*作者 ：WUYANGHAO*

*概述 ：从零开始编译一个属于自己的linux系统*
#### 目录
##### 一、[准备LiveCD宿主系统](ProductDoc/001-准备LiveCD宿主系统.md)
##### 二、[下载编译linux系统的软件包](ProductDoc/002-准备编译软件包.md)
##### 三、[编译工具链](ProductDoc/003-编译工具链.md)
##### 四、[编译操作系统](ProductDoc/004-编译操作系统.md)
##### 五、[完成系统配置](ProductDoc/005-完成系统配置.md)
##### 六、[设置系统启动](ProductDoc/006-设置系统启动.md)
##### 七、[完善系统网络配置](ProductDoc/007-完善系统网络配置.md)
##### 八、[完善系统软件包安装与管理](ProductDoc/008-完善系统软件包安装与管理.md)
##### 九、[安装GNOME3图形化桌面](ProductDoc/009-安装GNOME3图形化桌面.md)
##### 十、[美化GNOME3桌面](ProductDoc/010-美化GNOME3桌面.md)
#### 参考网站
##### 1、从零开始编译linux操作系统: <a href="http://www.linuxfromscratch.org/lfs/">linuxfromscratch官网</a>
##### 2、编译安装dpkg工具：编译<a href="http://http.debian.net/debian/pool/main/d/dpkg/dpkg_1.18.24.tar.xz">dpkg源代码</a>
##### 3、使用dpkg安装apt-get软件包管理器：所需的deb软件包见<a href="https://pkgs.org/">pkgs.org官网</a>
##### 4、使用apt-get安装GNOME桌面：<a href="https://www.gnome.org/">GNOME官网</a>